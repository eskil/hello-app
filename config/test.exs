import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :hello_app, HelloAppWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "LhBVCa4QLzdP65Be6YVBflpqLXWssBc+A2gitbn7FahYT8UQA325+DICgOtKfC/Z",
  server: false

config :hello_app, HelloAppWeb.PageController,
  title: "Test environment"

# In test we don't send emails.
config :hello_app, HelloApp.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
