defmodule HelloAppWeb.PageController do
  use HelloAppWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    title = Application.get_env(:hello_app, HelloAppWeb.PageController)[:title]
    render(conn, :home,
      title: title,
      layout: false)
  end
end
