# HelloApp

Pointless little json api service to test and play with ArgoCD
set. See the accompanying
[hello_app_config](https://gitlab.com/eskil/hello_app_config) repo for
the argocd setup and other argo related steps (eg. pushing docker
images).

## Gitlab

This was created as a blank gitlab project with "Registry (package or container)" as deployment target.

## Docker

The Dockerfile was created via `mix phx.gen.release --docker`, see [Phoenix Releases](https://hexdocs.pm/phoenix/releases.html)

See also [this logrocket blogpost](https://blog.logrocket.com/run-phoenix-application-docker/) for a walkthrough on running phoenix and docker.

This means the Dockerfile creates a release, not suitable for development and reloading.
See [Up and Running with Phoenix Live Reload and Docker](https://medium.com/@marksaystuff/up-and-running-with-phoenix-live-reload-and-docker-704a8031d49f)
or [Running Elixir in development with Docker and docker-compose](https://cohost.org/poffdeluxe/post/599167-running-elixir-in-de) for how to do that.

The docker compose setup runs the service plus it's postgres db.

```
# Launch service in docker with a secret
SECRET_KEY_BASE="HA/XD+hXzaL1+kV6KnB9yKf4abF9Xqs9swivkZNM+Gwj8zjwsYEhzacEKmIatQB1" docker compose up
```

## Kubernetes

[A Complete Guide to Deploying Elixir & Phoenix Applications on Kubernetes](https://medium.com/polyscribe/a-complete-guide-to-deploying-elixir-phoenix-applications-on-kubernetes-part-4-secret-f851d575bdd1) covers a lot, this section is about secrets.

```
```

## API

Here's some example curl statements that work against the docker
compose image (port `34000`). If you run the service locally or some
other way, you may need to use another port.

`mix phx.server` uses port 4000.
